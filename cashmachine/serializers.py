from django.contrib.auth import get_user_model

from rest_framework import serializers
from rest_framework.reverse import reverse

from .models import CardDetails, Operations, CardAccount


Card = get_user_model()


class CardAccountSerializer(serializers.ModelSerializer):

    link = serializers.SerializerMethodField('get_links')

    class Meta:
        model = CardAccount
        fields = ('id', Card.USERNAME_FIELD, 'is_active', 'link')

    def get_links(self, obj):
        request = self.context['request']
        username = obj.get_username()
        links = {
            'self': reverse('card-account-detail',
                            kwargs={Card.USERNAME_FIELD: username}, request=request),
            'card-detail': reverse('card-detail',
                                   kwargs={'pk': obj.pk}, request=request),
            'operations': reverse('operations-list', request=request)+'?card={}'.format(obj.pk)
        }
        return links


class CardDetailSerializer(serializers.ModelSerializer):

    link = serializers.SerializerMethodField('get_links')
    id = serializers.SerializerMethodField('get_card_id')
    card = serializers.SlugRelatedField(slug_field=CardAccount.USERNAME_FIELD, required=True,
                                        queryset=CardAccount.objects.order_by('card_number'))

    class Meta:
        model = CardDetails
        fields = ('id', 'card', 'money_amount', 'link')

    def get_card_id(self, obj):
        return obj.card.pk

    def get_links(self, obj):
        request = self.context['request']
        links = {
            'self': reverse('card-detail',
                            kwargs={'pk': obj.pk}, request=request),
            'card_account': None,
            'operations': None
        }
        if obj.card_id:
            links['card_account'] = reverse('card-account-detail',
                                            kwargs={Card.USERNAME_FIELD: obj.card.card_number}, request=request)
            links['operations'] = reverse('operations-list',
                                          request=request)+'?card={}'.format(obj.card_id)
        return links


class OperationSerializer(serializers.ModelSerializer):

    link = serializers.SerializerMethodField('get_links')

    class Meta:
        model = Operations
        fields = ('id', 'card', 'amount_delta', 'operation_type', 'time', 'link')

    def get_operation_type_display(self, obj):
        return obj.get_operation_type_display()

    def get_links(self, obj):
        request = self.context['request']
        links = {
            'self': reverse('operations-detail',
                            kwargs={'pk': obj.pk}, request=request),
            'card_account': reverse('card-account-detail',
                                    kwargs={Card.USERNAME_FIELD: obj.card.card_number}, request=request),
            'card_detail': reverse('card-detail',
                                   kwargs={'pk': obj.card_id}, request=request),
        }
        return links
