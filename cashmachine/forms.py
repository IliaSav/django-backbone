import django_filters

from .models import CardAccount, CardDetails, Operations


class CardAccountDetailsFilter(django_filters.FilterSet):

    class Meta:
        model = CardDetails
        fields = ('card', 'money_amount',)

    def filter_queryset(self, request, queryset, view):
        return queryset.filter(owner=request.user)


class OperationsFilter(django_filters.FilterSet):

    time_min = django_filters.DateFilter(name='time', lookup_type='gte')
    time_max = django_filters.DateFilter(name='time', lookup_type='lte')

    class Meta:
        model = Operations
        fields = ('card', 'operation_type', 'time_min', 'time_max',)
