from django.contrib.auth import get_user_model

from rest_framework import authentication, permissions, viewsets, filters

from .forms import CardAccountDetailsFilter, OperationsFilter
from .models import CardDetails, Operations, CardAccount
from .serializers import CardDetailSerializer, OperationSerializer, CardAccountSerializer


Card = get_user_model()


class IsOwnerFilterBackend(filters.BaseFilterBackend):
  """Filter that only allows users to see their own objects.
  """
  def filter_queryset(self, request, queryset, view):
    card = request.user
    try:
        return queryset.filter(card=card)
    except Exception:
        return queryset.filter(auth_token=card.auth_token)


class DefaultsMixin(object):
    authentication_classes = (
        authentication.BasicAuthentication,
        authentication.TokenAuthentication,
    )
    permission_classes = (
        permissions.IsAuthenticated,
    )
    paginate_by = 25
    paginate_by_param = 'page_size'
    max_paginate_by = 100
    filter_backends = (
        filters.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
        IsOwnerFilterBackend,
    )


class CardDetailViewSet(DefaultsMixin, viewsets.ModelViewSet):
    """API endpoint for card details"""

    queryset = CardDetails.objects.order_by('card')
    #queryset = CardDetails.objects.all()
    serializer_class = CardDetailSerializer
    filter_class = CardAccountDetailsFilter
    search_fields = ('card',)


class CardAccountViewSet(DefaultsMixin, viewsets.ReadOnlyModelViewSet):
    """API endpoint for card account"""

    lookup_field = Card.USERNAME_FIELD
    lookup_url_kwarg = Card.USERNAME_FIELD
    queryset = Card.objects.order_by(Card.USERNAME_FIELD)
    serializer_class = CardAccountSerializer
    search_fields = (Card.USERNAME_FIELD, )


class OperationViewSet(DefaultsMixin, viewsets.ModelViewSet):
    """API endpoint for operation viewset"""

    queryset = Operations.objects.order_by('time')
    serializer_class = OperationSerializer
    filter_class = OperationsFilter
    search_filed = ('card',)

