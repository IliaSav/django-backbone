from django.conf.urls import include, url
from django.views.generic import TemplateView

from rest_framework.routers import DefaultRouter

from . import views


router = DefaultRouter(trailing_slash=False)
router.register(r'card_accounts', views.CardAccountViewSet, base_name='card-account')
router.register(r'card_details', views.CardDetailViewSet, base_name='card')
router.register(r'operations', views.OperationViewSet, base_name='operations')

urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(r'^$', TemplateView.as_view(template_name='index.html'))
]