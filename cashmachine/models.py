from django.db import models, transaction
from django.db.models.signals import post_save
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.utils.translation import ugettext_lazy as _
from django.dispatch import receiver

from rest_framework.authtoken.models import Token

# Create your models here.


class CardAccountManager(BaseUserManager):
    def create_user(self, card_number, password='1111'):
        if not card_number:
            raise ValueError('Account must have card number.')

        card_account = self.model(
            card_number=card_number,
        )

        card_account.set_password(password)
        card_account.save(using=self._db)

        return card_account

    def create_superuser(self,  card_number, password='1111'):
        card_account = self.create_user(card_number, password)
        card_account.is_admin = True
        card_account.save(using=self._db)
        return card_account


# This code is triggered whenever a new user has been created and saved to the database
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        print("User created")
        Token.objects.create(user=instance)


class CardAccount(AbstractBaseUser):
    """Clients' account."""

    card_number = models.CharField(max_length=16, unique=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = CardAccountManager()

    USERNAME_FIELD = 'card_number'

    class Migration:

        needed_by = (
            ('authtoken', '0001_initial'),
        )

    def __unicode__(self):
        return self.card_number

    def __str__(self):
        return _('Card number: %s, is active: %s') % (self.card_number, self.is_active)

    def get_full_name(self):
        # The user is identified by their email address
        return self.card_number

    def get_short_name(self):
        # The user is identified by their email address
        return self.card_number


class CardDetails(models.Model):
    """Card account details."""
    card = models.OneToOneField(CardAccount, primary_key=True)
    money_amount = models.FloatField()

    def __str__(self):
        return _('%s, balance: %s') % (self.card, self.money_amount)


class Operations(models.Model):
    """Card operation logs."""

    BLOCK = 0
    INCOME = 1
    EXPENSE = 2
    BALANCE_CHECK = 3

    OPERATION_CHOICES = (
        (BLOCK, _('Card blocked')),
        (INCOME, _('Income')),
        (EXPENSE, _('Expense')),
        (BALANCE_CHECK, _('Check card balance')),
    )

    card = models.ForeignKey(CardAccount)
    amount_delta = models.FloatField(default=0)
    operation_type = models.SmallIntegerField(choices=OPERATION_CHOICES)
    time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return _('%s, operation code: %i') % \
                (self.card, self.operation_type,)

    @transaction.atomic
    def save(self, *args, **kwargs):
        if self.operation_type == self.EXPENSE:
            card_details = CardDetails.objects.get(card=self.card)
            if self.amount_delta>card_details.money_amount:
                raise ValueError("Operation fail. Not enough money.")
            if self.amount_delta <= 0:
                raise ValueError("Operation fail. Input value < 0.")
            card_details.money_amount -= self.amount_delta
            card_details.save()
        super().save(*args, **kwargs)

