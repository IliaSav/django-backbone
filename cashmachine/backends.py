from django.conf import settings
from django.contrib.auth.models import check_password
from .models import CardAccount

class CardAuthBackend(object):
    """Custom authentication backend."""

    def authenticate(self, card_number=None, password=None):
        """
        Authentication method
        """
        try:
            card = CardAccount.objects.get(card_number=card_number)
            if card.check_password(password):
                return card
        except CardAccount.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            card = CardAccount.objects.get(pk=user_id)
            if card.is_active:
                return card
            return None
        except CardAccount.DoesNotExist:
            return None