var addNav = function(base) {
    base.$el.addClass('current');
    $("body").css('padding-bottom', '250px'); // keep Donate Now button in view
    var inputs = $('input'),
        len = inputs.length - 1,
        indx = inputs.index(base.$el),
        topPadding = 50; // distance from top where the focused input is placed
    // make sure input is in view
    $(window).scrollTop(inputs.eq(indx).offset().top - topPadding);

    // see if nav is already set up
    if (base.$keyboard.find('.ui-keyboard-nav').length) {
        return;
    }

}; // end prev/next button code

$('.keyboard-num').keyboard({
    layout: 'custom',
    autoAccept: 'true',
    customLayout: {
        'default': [
            '7 8 9',
            '4 5 6',
            '1 2 3',
            '0 {bksp}',
            '{accept}'
            ]
    },
    usePreview: false,
    visible: function(e, keyboard, el) {
        addNav(keyboard);
    },
    beforeClose: function(e, keyboard, el, accepted) {
        $('input.current').removeClass('current');
        $("body").css('padding-bottom', '0px');
    }
});