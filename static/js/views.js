(function ($, Backbone, _, app) {

    var TemplateView = Backbone.View.extend({
        templateName: '',
        initialize: function () {
            this.template = _.template($(this.templateName).html());
        },
        render: function () {
            var context = this.getContext(),
                html = this.template(context);
            this.$el.html(html);
        },
        getContext: function () {
            return {};
        }
    });

    var FormView = TemplateView.extend({
        events: {
            'submit form': 'submit'
        },
        errorTemplate: _.template('<span class="error"><%- msg %></span>'),
        clearErrors: function () {
            $('.error', this.form).remove();
        },
        showErrors: function (errors) {
            _.map(errors, function (fieldErrors, name) {
                var field = $(':input[name=' + name + ']', this.form),
                    label = $('label[for=' + field.attr('id') + ']', this.form);
                if (label.length === 0) {
                    label = $('label', this.form).first();
                }
                function appendError(msg) {
                    label.before(this.errorTemplate({msg: msg}));
                }
                _.map(fieldErrors, appendError, this);
            }, this);
        },
        serializeForm: function (form) {
            return _.object(_.map(form.serializeArray(), function (item) {
                // Convert object to tuple of (name, value)
                return [item.name, item.value];
            }));
        },
        submit: function (event) {
            event.preventDefault();
            this.form = $(event.currentTarget);
            this.clearErrors();
        },
        failure: function (xhr, status, error) {
            var errors = xhr.responseJSON;
            this.showErrors(errors);
        },
        done: function (event) {
            if (event) {
                event.preventDefault();
            }
            this.trigger('done');
            this.remove();
        },
        // page 138, page 139
        modelFailure: function (model, xhr, options) {
            var errors = xhr.responseJSON;
            this.showErrors(errors);
        }
    });


    var HomepageView = TemplateView.extend({
        templateName: '#home-template',
        initialize: function (options) {
           // this.model.on('error', this.render, this);
            var self = this;
            TemplateView.prototype.initialize.apply(this, arguments);
        }
    });


    var LoginView = FormView.extend({
        id: 'login',
        templateName: '#login-template',

        submit: function (event) {
            var data = {};
            FormView.prototype.submit.apply(this, arguments);
            data = this.serializeForm(this.form);
            // Submit the login form
            $.post(app.apiLogin, data)
                .success($.proxy(this.loginSuccess, this))
                .fail($.proxy(this.failure, this));
        },
        loginSuccess: function (data) {
            app.session.save(data.token);
            this.done();
        }
    });

    var HeaderView = TemplateView.extend({
        tagName: 'header',
        templateName: '#header-template',
        events: {
            'click a.logout': 'logout'
        },
        getContext: function () {
            return {authenticated: app.session.authenticated()};
        },
        logout: function (event) {
            event.preventDefault();
            app.session.delete();
            window.location = '/';
        }
    });

    var CardDetailsView = TemplateView.extend({
        templateName: '#balance',
        initialize: function(options){
            var self = this;
            var data = {};
            TemplateView.prototype.initialize.apply(this, arguments);
            this.card_details = null;
            app.collections.ready.done(function(){
                app.card_details.getOrFetch().
                    done(function(card_details){
                        self.card_details = card_details.get('0');
                        data["card"] = self.card_details["id"];
                        data["operation_type"] = 3;     //Check card balance.
                        app.operations.create(data,{
                            wait: true,
                            success: $.proxy(self.success, self),
                            error: $.proxy(self.modelFailure, self)
                        });
                        self.render();
                    }).fail(function(card_details){
                        self.card_details = card_details;
                        self.card_details.invalid = true;
                        self.render();
                    });
            });
        },
        getContext: function(){
            return {card_details: this.card_details}
        }
    });


    var GetCashView = FormView.extend({
        templateName: '#get-cash',
        initialize: function(options){
            var self = this;
            TemplateView.prototype.initialize.apply(this, arguments);
            self.render();
        },
        submit: function (event) {
            var self = this;
            var data = {};
            FormView.prototype.submit.apply(this, arguments);
            data = this.serializeForm(this.form);
            app.collections.ready.done(function(){
                app.card_accounts.getOrFetch().
                    done(function(card_accounts){
                        var card_account = card_accounts.get('0');
                        data["card"]=card_account["id"];
                        data["operation_type"]=2;
                        var amount = data["amount_delta"];
                        app.operations.create(data,{
                            wait: true,
                            success: $.proxy(self.success(amount), self),
                            error: $.proxy(self.modelFailure, self)
                        });
                        //self.render();
                    }).fail(function(card_accounts){
                        self.card_accounts = card_accounts;
                        self.card_accounts.invalid = true;
                        self.render();
                    });

            });
        },
        success: function (amount) {
            window.location = '/#info/'+amount;
        }
    });

    var InfoView = TemplateView.extend({
        templateName: '#info',
        initialize: function(options){
            var self = this;
            self.card_details = null;
            var data = {};
            TemplateView.prototype.initialize.apply(this, arguments);
            this.card_details = null;
            app.collections.ready.done(function(){
                app.card_details.getOrFetch().
                    done(function(card_details){
                        self.card_details = card_details.get('0');
                        //data["card"] = self.card_details["id"];
                        self.card_details["amount"] = options.amount;
                        self.render();
                    }).fail(function(card_details){
                        self.card_details = card_details;
                        self.card_details.invalid = true;
                        self.render();
                });
            });
        },
        getContext: function(){
            return {card_details: this.card_details}
        }
    });

    var ErrorView = TemplateView.extend({
        templateName: '#error',
        initialize: function (options) {
            var self = this;
            TemplateView.prototype.initialize.apply(this, arguments);
            self.render();
        }
    });

    app.views.HomepageView = HomepageView;
    app.views.LoginView = LoginView;
    app.views.HeaderView = HeaderView;
    app.views.CardDetailsView = CardDetailsView;
    app.views.GetCashView = GetCashView;
    //app.veiws.ErrorView = ErrorView;
    app.views.InfoView = InfoView;

})(jQuery, Backbone, _, app);