from django.conf.urls import include, url

from rest_framework.authtoken.views import obtain_auth_token

# from ..cashmachine.urls import router


urlpatterns = [
    # url(r'^api/token/', obtain_auth_token, name='api-token'),
    url(r'^', include('cashmachine.urls'))
]

urlpatterns += [
    url(r'^api-token-auth/', obtain_auth_token, name='api-token')
]